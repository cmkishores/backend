# Generated by Django 2.2.6 on 2019-10-26 13:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Camp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Name', max_length=20)),
                ('location', models.CharField(help_text='Location name', max_length=200)),
                ('latitude', models.CharField(help_text='Latitude', max_length=50)),
                ('longitude', models.CharField(help_text='Longitude', max_length=50)),
                ('contact_info', models.CharField(help_text='Contact_info', max_length=50)),
                ('incharge', models.CharField(help_text='Camp incharge', max_length=40)),
                ('phone', models.CharField(help_text='Camp phone number', max_length=15)),
                ('photo', models.ImageField(blank=True, null=True, upload_to='camps/%Y/%m/%d/')),
                ('capacity', models.IntegerField(help_text='The max capacity of the camp')),
                ('number_of_people', models.IntegerField(default=0, help_text='The number of people currently in the camp')),
            ],
        ),
    ]
